const express = require("express");
var bodyParser = require('body-parser');
const app = express();
const userRouter = require("./routes/userRoutes");
const noteRouter = require("./routes/noteRoutes");
const mongoose = require("mongoose");
const dotenv = require("dotenv");
const cors = require("cors");
const profileRouter = require("./routes/profileRoutes");


dotenv.config();
app.use(cors());

app.use(express.json());



//use Router by using app.use()
app.use("/users", userRouter);
app.use("/note", noteRouter);
app.use("/api", profileRouter);



app.get("/", (req, res) => {
    res.send("WellCome to Note Api");
})


const PORT = process.env.PORT || 5000;


//connect mongodatabase
mongoose.connect("mongodb+srv://admin:admin@cluster0.3mrmr.mongodb.net/?retryWrites=true&w=majority")
    .then(() => {

        //start servers
        app.listen(PORT, () => {
            console.log("server start on port no " + PORT);
        });

    })
    .catch((error) => {
        console.log(error)
    })

