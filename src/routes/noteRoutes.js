const expres = require("express");
const { model } = require("mongoose");
const { getNotes, createNote, updateNote, deleteNote, getMyNotes } = require("../controllers/noteController");
const auth = require("../middlewares/auth");
const noteRouter = expres.Router();


noteRouter.get("/", auth, getNotes);

noteRouter.get("/me", getMyNotes)


noteRouter.post("/", auth, createNote);

noteRouter.put("/:id", auth, updateNote);

noteRouter.delete("/:id", auth, deleteNote);

module.exports = noteRouter;