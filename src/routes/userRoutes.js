const expres = require("express");
const { signup, signin, profile, uploadImage } = require("../controllers/userController");
const profile_image = require("../middlewares/file_upload");
const userRouter = expres.Router();


userRouter.post("/signup", signup)

userRouter.post("/signin", signin);

userRouter.post("/profile", profile_image, profile);




module.exports = userRouter;