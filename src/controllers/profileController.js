const ProfileModel = require("../models/profileModel");
const bcrypt = require('bcrypt');
const jwt = require("jsonwebtoken");
const SECREAT_KEY = "NOTEAPI";

const createProfile = async (req, res,) => {

    try {

        //Hasshed Password
        const hashedPassword = await bcrypt.hash(req.body.password, 10);

        const profile = new ProfileModel({
            username: req.body.username,
            email: req.body.email,
            password: hashedPassword,
            mobile: req.body.mobile,
            image: req.file.filename,
            type: req.body.type,
        });

        //ceck existig user 
        const existingUser = await ProfileModel.findOne({ email: profile.email });
        if (existingUser) {
            res.status(300).send({ message: "User already exist" });
        }
        else {
            //save data to dataase
            const profile_data = await profile.save();

            //Token Generation
            const token = jwt.sign({
                email: profile_data.email,
                id: profile_data.id,
            }, SECREAT_KEY);

            res.status(200).send({ success: true, data: profile_data, token: token });

        }
    }
    catch (error) {
        res.status(400).send(error.message);
    }

}

module.exports = {
    createProfile
};
