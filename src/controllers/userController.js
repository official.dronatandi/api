const userModel = require("../models/user");
const bcrypt = require("bcrypt");
const jwt = require("jsonwebtoken");
var fs = require("fs");
var path = require('path');

const SECREAT_KEY = "NOTEAPI";

const signup = async (req, res) => {

    const { username, email, password } = req.body;
    try {
        //Existing User check

        const existingUser = await userModel.findOne({ email });
        if (existingUser) {
            return res.status(300).json({ message: "User already exist" });
        }

        //Hasshed Password
        const hashedPassword = await bcrypt.hash(password, 10);

        //User Creation
        const result = await userModel.create({
            email: email,
            password: hashedPassword,
            username: username
        });

        //Token Generation
        const token = jwt.sign({
            email: result.email,
            id: result._id,
        }, SECREAT_KEY);

        res.status(200).json({ user: result, token: token });



    }
    catch (error) {

        console.log(error);
        res.status(500).json({ message: "Something went wrong" });

    }


}



const signin = async (req, res) => {
    const { email, password } = req.body;

    try {
        const existingUser = await userModel.findOne({ email: email });
        if (!existingUser) {
            return res.status(404).json({ message: "User not found" });
        }

        const matchPassword = await bcrypt.compare(password, existingUser.password);

        if (!matchPassword) {
            return res.status(404).json({ message: "Invalid Credential....." });
        }

        const token = jwt.sign({ email: existingUser.email, id: existingUser._id }, SECREAT_KEY);
        res.status(200).json({ user: existingUser, token: token });

    }
    catch (error) {
        console.log(error);
        res.status(500).json({ message: "Something went wrong" });

    }

}


const profile = async (req, res) => {
    res.send("uploaded");

}

module.exports = { signin, signup, profile};