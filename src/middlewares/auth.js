const jwt = require("jsonwebtoken");
const SECRETE_KEY = "NOTEAPI";


const auth = (req, res, next) => {

    try {

        let token = req.headers.authorization;
        if (token) {
            token = token.split(" ")[1];
            let user = jwt.verify(token, SECRETE_KEY);
            req.userId = user.id;
        }
        else {
            res.status(401).json({ message: "Unauthorized User" });
        }

        next();

    }

    catch (error) {
        console.log(error);
        res.status(401).json({ message: "Unautherized User" });
    }
}

module.exports = auth;